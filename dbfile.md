1.\sql

2.\connect root@localhost

3.create database  IoTExam;

4. use IoTExam;

5.create table ldr (id integer primary key auto_increment,value float,date TIMESTAMP default CURRENT_TIMESTAMP);

6. create table led(id integer primary key auto_increment,boolean_status float,date TIMESTAMP default CURRENT_TIMESTAMP);

7.show tables;
+-------------------+
| Tables_in_iotexam |
+-------------------+
| ldr               |
| led               |
+-------------------+
##insert ldr values;
8.select id,value,date from ldr;
+----+-------+---------------------+
| id | value | date                |
+----+-------+---------------------+
|  1 |    90 | 2021-07-22 08:53:29 |
+----+-------+---------------------+
9. select id,value,date from ldr;
+----+-------+---------------------+
| id | value | date                |
+----+-------+---------------------+
|  1 |    90 | 2021-07-22 08:53:29 |
|  2 |    91 | 2021-07-22 08:54:09 |
+----+-------+---------------------+
10.select id,value,date from ldr;
+----+-------+---------------------+
| id | value | date                |
+----+-------+---------------------+
|  1 |    90 | 2021-07-22 08:53:29 |
|  2 |    91 | 2021-07-22 08:54:09 |
|  3 |    92 | 2021-07-22 08:54:21 |
+----+-------+---------------------+

###insert and update led

11.select id,status,date from led;
+----+--------+---------------------+
| id | status | date                |
+----+--------+---------------------+
|  1 | on     | 2021-07-22 09:17:54 |
+----+--------+---------------------+
